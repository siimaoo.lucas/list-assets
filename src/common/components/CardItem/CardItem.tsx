import { Card } from 'antd';
import React from 'react';

type CardItemProps = {
  type: 'card' | 'list';
  title: string;
  children?: React.ReactNode;
  onClick?: React.MouseEventHandler<HTMLDivElement>;
  style?: React.CSSProperties;
};

function CardItem({ children, title, type, onClick, style }: CardItemProps) {
  const DEFAULT_STYLE: React.CSSProperties = {
    margin: '0 auto',
    borderRadius: '1rem',
    width: '95%',
    overflow: 'hidden',
  };
  const CARD_STYLE: React.CSSProperties = {
    ...DEFAULT_STYLE,
    maxWidth: '400px',
    ...style,
  };
  const LIST_STYLE: React.CSSProperties = { ...DEFAULT_STYLE, ...style };

  return (
    <Card
      style={type === 'card' ? CARD_STYLE : LIST_STYLE}
      onClick={onClick}
      role="listitem"
    >
      <h2>{title}</h2>
      {children}
    </Card>
  );
}

export default CardItem;
