import React from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { Dropdown, Menu, PageHeader } from 'antd';
import {
  HomeOutlined,
  SelectOutlined,
  TagsOutlined,
  UserOutlined,
  TranslationOutlined,
  ArrowLeftOutlined,
} from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

type LayoutProps = {
  children: React.ReactNode;
};

type ApplicationRoutes = keyof typeof ROUTES_DATA;

export const ROUTES_DATA = {
  users: {
    title: 'layout.users',
    path: 'users',
    icon: <UserOutlined />,
  },
  companies: {
    title: 'layout.companies',
    path: 'companies',
    icon: <HomeOutlined />,
  },
  units: {
    title: 'layout.units',
    path: 'units',
    icon: <SelectOutlined />,
  },
  assets: {
    title: 'layout.assets',
    path: 'assets',
    icon: <TagsOutlined />,
  },
};

function Layout({ children }: LayoutProps) {
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const currentRoute = pathname.split('/')[1] as ApplicationRoutes;

  const { t, i18n } = useTranslation('common');
  return (
    <div>
      <PageHeader
        onBack={() => navigate(-1)}
        backIcon={!!ROUTES_DATA[currentRoute]?.title && <ArrowLeftOutlined />}
        title={t(ROUTES_DATA[currentRoute]?.title) || 'TRACTIAN'}
        style={{
          position: 'fixed',
          zIndex: 2,
          background: '#fff',
          width: ' 100%',
          display: 'flex',
          justifyContent: 'space-between',
        }}
      >
        <Dropdown
          overlay={
            <Menu>
              <Menu.Item
                key={0}
                onClick={() => i18n.changeLanguage('en')}
              >
                {t('layout.languages.english')}
              </Menu.Item>
              <Menu.Item
                key={1}
                onClick={() => i18n.changeLanguage('pt')}
              >
                {t('layout.languages.portuguese')}
              </Menu.Item>
            </Menu>
          }
        >
          <TranslationOutlined style={{ fontSize: '1.4rem' }} />
        </Dropdown>
      </PageHeader>

      <div style={{ paddingTop: '72px' }}>{children}</div>
    </div>
  );
}

export default Layout;
