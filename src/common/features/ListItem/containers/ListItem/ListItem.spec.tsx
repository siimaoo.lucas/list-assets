import React from 'react';
import { render, waitFor } from '@testing-library/react';

import { rest } from 'msw';
import { setupServer } from 'msw/node';

import ListItem from './ListItem';

let body: Array<unknown> = [];

const server = setupServer(
  rest.get('http://localhost', (_, res, ctx) => {
    return res(ctx.status(200), ctx.json(body));
  }),
);

const generateUsers = (quantity: 3) => {
  return Array(quantity)
    .fill(null)
    .map((_, index) => {
      return {
        id: index,
        email: `teste${index}@tractian.com`,
        name: `Testador ${index}`,
        unitId: 1,
        companyId: 1,
      };
    });
};

describe('ListItem Container', () => {
  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  it('should render an empty table if data was not provided', async () => {
    const { queryByRole } = render(<ListItem requestURL="" />);
    await waitFor(() =>
      expect(queryByRole('listitem')).not.toBeInTheDocument(),
    );
  });

  it('should render a table with three rows', async () => {
    body = [...generateUsers(3)];
    const { getAllByRole } = render(<ListItem />);
    await waitFor(() => expect(getAllByRole('listitem').length).toBe(3));
  });
});

export default {};
