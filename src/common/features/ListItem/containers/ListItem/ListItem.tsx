import React, { useState } from 'react';
import { useRequest } from 'common/hooks';
import { Spin, Card as CardAnt, Button } from 'antd';
import { CardItem } from 'common/components';
import { AppstoreOutlined, InsertRowBelowOutlined } from '@ant-design/icons';

const Card = ({
  type,
  data,
}: {
  type: 'card' | 'list';
  data: { name: string };
}) => (
  <CardItem
    type={type}
    title={data.name}
  />
);

type ListItemProps = {
  requestURL?: string;
  onClick?: (record: { id: string | number }) => void;
  Component?: React.ComponentType<
    {
      onClick?: (record: { id: string | number }) => void;
      type: 'card' | 'list';
    } & any
  >;
};

function ListItem({ requestURL, onClick, Component = Card }: ListItemProps) {
  const { data, loading } = useRequest<Array<Record<string, any>>>({
    url: requestURL,
    method: 'GET',
  });

  const [listType, setListType] = useState<'card' | 'list'>(
    (localStorage?.getItem('listType') as 'card' | 'list') || 'card',
  );

  const handleSetListType = (type: 'card' | 'list') => {
    localStorage.setItem('listType', type);
    setListType(type);
  };

  return (
    <section
      style={{
        display: 'grid',
        placeItems: 'center',
        gridTemplateColumns: '100%',
        gap: '1rem',
      }}
    >
      {loading ? (
        <Spin />
      ) : (
        <>
          <CardAnt
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              width: '100%',
              borderRadius: '1rem',
            }}
          >
            <Button
              disabled={listType === 'card'}
              shape="round"
              onClick={() => handleSetListType('card')}
            >
              <AppstoreOutlined />
            </Button>
            <Button
              disabled={listType === 'list'}
              shape="round"
              onClick={() => handleSetListType('list')}
            >
              <InsertRowBelowOutlined />
            </Button>
          </CardAnt>

          <div
            style={{
              position: 'relative',
              overflowX: 'auto',
              width: '100%',
              display: 'grid',
              gap: '1rem',
              marginBottom: '1rem',
              ...(listType === 'card'
                ? {
                    gridTemplateColumns:
                      'repeat(auto-fit, minmax(300px, 400px))',
                    justifyContent: 'center',
                  }
                : {}),
            }}
          >
            {data?.map((item: any) => {
              return (
                <Component
                  key={item.id}
                  onClick={onClick}
                  type={listType}
                  data={item}
                />
              );
            })}
          </div>
        </>
      )}
    </section>
  );
}

export default ListItem;
