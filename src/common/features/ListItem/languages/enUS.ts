export default {
  emptyText: 'No data',
  cancelSort: 'Cancel',
  triggerAsc: 'Sort Ascending',
  triggerDesc: 'Sort Descending',
  id: 'ID',
  model: 'Model',
  status: 'Status',
  healthscore: 'Health Score',
  name: 'Name',
  unitId: 'Unit ID',
  companyId: 'Company ID',
  email: 'Email',
  assets: {
    inOperation: 'In Operation',
    inDowntime: 'In Downtime',
    inAlert: 'In Alert',
  },
};
