export default {
  emptyText: 'Nenhum dado',
  cancelSort: 'Limpar ordenação',
  triggerAsc: 'Ordem crescente',
  triggerDesc: 'Ordem decrescente',
  id: 'ID',
  model: 'Modelo',
  status: 'Status',
  healthscore: 'Saúde',
  name: 'Nome',
  unitId: 'ID da unidade',
  companyId: 'ID da empresa',
  email: 'E-mail',
  assets: {
    inOperation: 'Em Operação',
    inDowntime: 'Em Parada',
    inAlert: 'Em Alerta',
  },
};
