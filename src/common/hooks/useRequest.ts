import { AxiosRequestConfig, AxiosPromise } from 'axios';
import { http } from 'common/utils';
import { useEffect, useState } from 'react';

const useRequest = <T, E = Error>(config: AxiosRequestConfig<unknown>) => {
  type Result = {
    data: T | null;
    error: E | Error | null;
    loading: boolean;
  };

  const [result, setResult] = useState<Result>({
    loading: false,
    error: null,
    data: null,
  });

  useEffect(() => {
    const request = async () => {
      try {
        setResult({ ...result, loading: true });
        const response = await (<AxiosPromise<T>>http(config));
        setResult({ ...result, data: response?.data, loading: false });
      } catch (err) {
        setResult({ ...result, error: err as E, loading: false });
      }
    };

    request();
  }, []);

  return { ...result };
};

export default useRequest;
