import { useEffect, useRef } from 'react';

const useTitle = (title: string) => {
  const isDocumentDefined = typeof document !== 'undefined';
  const originalTitle = useRef(isDocumentDefined ? document.title : '');

  useEffect(() => {
    if (!isDocumentDefined) return;

    if (document.title !== title) document.title = `${title} | TRACTIAN`;

    return () => {
      document.title = originalTitle.current;
    };
  }, [title]);
};

export default useTitle;
