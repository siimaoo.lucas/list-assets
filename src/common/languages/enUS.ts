import ListItemLanguage from '../features/ListItem/languages/enUS';

export default {
  layout: {
    users: 'Users',
    companies: 'Companies',
    units: 'Units',
    assets: 'Assets',
    languages: {
      english: 'English',
      portuguese: 'Portuguese',
    },
  },
  listItem: {
    ...ListItemLanguage,
  },
};
