import ListItemLanguage from '../features/ListItem/languages/ptBR';

export default {
  layout: {
    users: 'Usuarios',
    companies: 'Empresas',
    units: 'Unidades',
    assets: 'Ativos',
    languages: {
      english: 'Ingles',
      portuguese: 'Portugues',
    },
  },
  listItem: {
    ...ListItemLanguage,
  },
};
