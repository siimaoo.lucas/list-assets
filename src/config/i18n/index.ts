import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import enUS from './languages/enUS';
import ptBR from './languages/ptBR';

const resources = {
  en: { ...enUS },
  pt: { ...ptBR },
};

i18n.use(initReactI18next).init({
  resources,
  lng: 'pt',
  fallbackLng: 'pt',
  interpolation: {
    escapeValue: false,
  },
  debug: process.env.NODE_ENV === 'development',
  ns: ['common'],
});

export default i18n;
