import CommonTranslations from 'common/languages/enUS';
import AssetsTranslations from 'routes/Assets/languages/enUS';
import CompaniesTranslations from 'routes/Companies/languages/enUS';
import HomeTranslations from 'routes/Home/languages/enUS';
import UnitsTranslations from 'routes/Units/languages/enUS';
import UsersTranslations from 'routes/Users/languages/enUS';

export default {
  common: {
    ...CommonTranslations,
  },
  assets: {
    ...AssetsTranslations,
  },
  companies: {
    ...CompaniesTranslations,
  },
  home: {
    ...HomeTranslations,
  },
  units: {
    ...UnitsTranslations,
  },
  users: {
    ...UsersTranslations,
  },
};
