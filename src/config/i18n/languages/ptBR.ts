import CommonTranslations from 'common/languages/ptBR';
import AssetsTranslations from 'routes/Assets/languages/ptBR';
import CompaniesTranslations from 'routes/Companies/languages/ptBR';
import HomeTranslations from 'routes/Home/languages/ptBR';
import UnitsTranslations from 'routes/Units/languages/ptBR';
import UsersTranslations from 'routes/Users/languages/ptBR';

export default {
  common: {
    ...CommonTranslations,
  },
  assets: {
    ...AssetsTranslations,
  },
  companies: {
    ...CompaniesTranslations,
  },
  home: {
    ...HomeTranslations,
  },
  units: {
    ...UnitsTranslations,
  },
  users: {
    ...UsersTranslations,
  },
};
