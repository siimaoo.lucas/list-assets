import React from 'react';
import { Routes, Route, BrowserRouter as Router } from 'react-router-dom';

import './common/assets/css/_app.css';
import 'antd/dist/antd.css';

import { Layout } from 'common/components';

import Users from 'routes/Users/views/Users';
import Companies from 'routes/Companies/views/Companies';
import Units from 'routes/Units/views/Units';
import Assets from 'routes/Assets/views/Assets';
import Home from 'routes/Home/views/Home';
import Asset from 'routes/Assets/views/Asset';

function App() {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route
            path="/users"
            element={<Users />}
          />

          <Route
            path="/companies"
            element={<Companies />}
          />

          <Route
            path="/units"
            element={<Units />}
          />

          <Route
            path="/assets"
            element={<Assets />}
          />

          <Route
            path="/assets/:id"
            element={<Asset />}
          />

          <Route
            path="/"
            element={<Home />}
          />
        </Routes>
      </Layout>
    </Router>
  );
}

export default App;
