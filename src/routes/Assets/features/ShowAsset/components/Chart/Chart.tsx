import React, { useEffect } from 'react';
import { chart, Options } from 'highcharts';
import { Card } from 'antd';

function Chart({ chartOptions }: { chartOptions: Options }) {
  const chartRef = React.createRef<HTMLDivElement>();
  useEffect(() => {
    if (chartRef.current) {
      chart(chartRef.current, chartOptions);
    }
  }, []);

  return (
    <Card style={{ width: '100%', borderRadius: '1rem' }}>
      <div ref={chartRef} />
    </Card>
  );
}

export default Chart;
