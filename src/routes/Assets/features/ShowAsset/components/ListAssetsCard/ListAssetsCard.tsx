import React from 'react';
import { CardItem } from 'common/components';
import { Avatar, Image, Statistic, Tag, Tooltip } from 'antd';
import { useTranslation } from 'react-i18next';
import {
  DashboardOutlined,
  FireOutlined,
  ThunderboltOutlined,
} from '@ant-design/icons';
import { Asset } from '../../containers/ShowAsset/ShowAsset';

type ListAssetsCardProps = {
  type: 'card' | 'list';
  onClick?: (record: { id: string | number }) => void;
  children?: React.ReactNode;
  style?: React.CSSProperties;
} & { data: Asset };

const BADGE_COLOR = {
  inOperation: 'blue',
  inAlert: 'orange',
  inDowntime: 'red',
};

type Badges = keyof typeof BADGE_COLOR;

const SPECS_STYLES = {
  display: 'flex',
  gap: '0.3rem',
  fontSize: '1rem',
  alignItems: 'center',
};

function ListAssetsCard({
  type,
  onClick,
  data,
  children,
  style,
}: ListAssetsCardProps) {
  const { t } = useTranslation('assets');

  return (
    <CardItem
      type={type}
      title={data?.name}
      onClick={() => onClick?.({ id: data?.id })}
      style={style}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: type === 'card' ? 'column' : 'row',
          alignItems: type === 'card' ? 'unset' : 'center',
          gap: '0.3rem',
        }}
      >
        {type === 'card' ? (
          <>
            <div
              style={{
                objectFit: 'cover',
                width: '100%',
                height: '200px',
                marginBottom: '0.8rem',
                overflow: 'hidden',
                borderRadius: '1rem',
              }}
            >
              <img
                src={data?.image}
                alt={data?.name}
                style={{ width: '100%' }}
              />
            </div>
            <Statistic
              title={t('showAsset.charts.healthScore')}
              value={data?.healthscore}
              suffix="/ 100"
            />
          </>
        ) : (
          <>
            <Avatar
              src={data?.image}
              alt={data?.name}
              size={50}
              style={{ minWidth: '50px' }}
            />
            <span>
              <strong>{t('showAsset.charts.healthScore')}</strong>:{' '}
              {data?.healthscore}/100
            </span>
          </>
        )}

        <strong>
          Status:{' '}
          <Tag color={BADGE_COLOR[data?.status as Badges]}>
            {t(`showAsset.card.status.${data?.status}`)}
          </Tag>
        </strong>

        <span style={{ display: 'flex', gap: '0.3rem', fontSize: '0.8rem' }}>
          <span style={SPECS_STYLES}>
            <Tooltip title={t('showAsset.card.maxTemp')}>
              <FireOutlined />
              {data?.specifications.maxTemp || 'N/A'}
            </Tooltip>
          </span>
          <span style={SPECS_STYLES}>
            <Tooltip title={t('showAsset.card.power')}>
              <ThunderboltOutlined />
              {data?.specifications.power || 'N/A'}
            </Tooltip>
          </span>
          <span style={SPECS_STYLES}>
            <Tooltip title={t('showAsset.card.rpm')}>
              <DashboardOutlined />
              {data?.specifications.rpm || 'N/A'}
            </Tooltip>
          </span>
        </span>

        {children}
      </div>
    </CardItem>
  );
}

export default ListAssetsCard;
