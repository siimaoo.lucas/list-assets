import React from 'react';
import { render, waitFor } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import ShowAsset from './ShowAsset';

const body = {
  id: 6,
  sensors: ['MOE1378'],
  status: 'inDowntime',
  healthscore: 91.2,
  model: 'fan',
  name: 'Ventilador D22',
  image:
    'https://tractian-img.s3.amazonaws.com/2f7eb04cfa255ab00088534f7d51f6f4.jpeg',
  metrics: {
    totalCollectsUptime: 6231,
    totalUptime: 1542.8288661110903,
    lastUptimeAt: '2021-02-16T16:23:58.801Z',
  },
  specifications: {
    rpm: 832,
    maxTemp: 58,
    power: 1.5,
  },
  unitId: 2,
  companyId: 1,
};

const server = setupServer(
  rest.get('http://localhost/assets/6', (_, res, ctx) => {
    return res(ctx.status(200), ctx.json(body));
  }),
);

jest.mock('react-router-dom', () => ({
  useParams: () => ({
    id: 6,
  }),
}));

describe('ShowAsset container', () => {
  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  it('should render all asset data', async () => {
    const { getByText, getByRole } = render(<ShowAsset />);
    await waitFor(() => expect(getByText(`${body.name}`)).toBeTruthy());
    expect(getByRole('listitem')).toBeTruthy();
  });
});
