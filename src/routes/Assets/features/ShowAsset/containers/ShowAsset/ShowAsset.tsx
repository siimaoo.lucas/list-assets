import React from 'react';
import { useRequest, useTitle } from 'common/hooks';
import { useParams } from 'react-router-dom';
import { Button, Spin, Alert } from 'antd';
import { Options } from 'highcharts';
import { useTranslation } from 'react-i18next';
import { Chart, ListAssetsCard } from '../../components';

type Specifications = {
  maxTemp?: number;
  rpm?: number;
  power?: number;
};

type Metrics = {
  totalCollectsUptime: number;
  totalUptime: number;
  lastUptimeAt: Date;
};

export type Asset = {
  id: number;
  sensors: string[];
  model: string;
  status: string;
  healthscore: number;
  name: string;
  image: string;
  specifications: Specifications;
  metrics: Metrics;
  unitId: number;
  companyId: number;
};

function ShowAsset() {
  const { id } = useParams();

  const { data, loading } = useRequest<Asset>({
    url: `/assets/${id}`,
    method: 'GET',
  });

  const { t } = useTranslation('assets');

  const charts: Array<Options> = [
    {
      chart: { type: 'column' },
      title: { text: t('showAsset.charts.collectUpTime') },
      series: [
        {
          type: 'column',
          data: [{ y: data?.metrics?.totalCollectsUptime }],
          name: t('showAsset.charts.collectUpTime'),
        },
      ],
    },
    {
      chart: { type: 'column' },
      title: { text: t('showAsset.charts.hoursCollectUpTime') },
      series: [
        {
          type: 'column',
          data: [{ y: Number(data?.metrics?.totalUptime.toFixed(2)) }],
          name: t('showAsset.charts.hoursCollectUpTime'),
        },
      ],
    },
    {
      chart: { type: 'column' },
      title: { text: t('showAsset.charts.healthScore') },
      series: [
        {
          type: 'column',
          data: [{ y: Number(data?.healthscore) }],
          name: t('showAsset.charts.healthScore'),
        },
      ],
      yAxis: { max: 100, labels: { format: '{value}%' } },
    },
  ];

  useTitle(data?.name || '');

  return (
    <section
      style={{
        display: 'grid',
        placeItems: 'center',
        gridTemplateColumns: '100%',
        margin: '0 auto',
        gap: '0.8rem',
        padding: '0 1rem',
      }}
    >
      {loading ? (
        <Spin />
      ) : (
        <div
          style={{
            display: 'grid',
            width: '100%',
            gridTemplateColumns: 'repeat(auto-fit, minmax(300px, 400px))',
            gap: '0.8rem',
            justifyContent: 'center',
          }}
        >
          <ListAssetsCard
            data={data as Asset}
            type="card"
          />

          {charts.map((chart) => (
            <Chart
              key={chart.title?.text}
              chartOptions={chart}
            />
          ))}
        </div>
      )}
    </section>
  );
}

export default ShowAsset;
