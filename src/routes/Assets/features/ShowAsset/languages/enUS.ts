export default {
  charts: {
    collectUpTime: 'Total collects uptime',
    hoursCollectUpTime: 'Total hours collects uptime',
    healthScore: 'Health score',
  },
  card: {
    name: 'Name',
    model: 'Model',
    sensors: 'Sensors',
    specifications: 'Specifications',
    maxTemp: 'Max temp',
    power: 'Power',
    rpm: 'RPM',
    status: {
      inDowntime: 'In downtime',
      inAlert: 'In alert',
      inOperation: 'In operation',
    },
  },
};
