export default {
  charts: {
    collectUpTime: 'Total de Coletas Uptime(Ligada)',
    hoursCollectUpTime: 'Total de Horas de Coletas Uptime(Ligada)',
    healthScore: 'Saúde',
  },
  card: {
    name: 'Nome',
    model: 'Modelo',
    sensors: 'Sensores',
    specifications: 'Especificações',
    maxTemp: 'Temp máxima',
    power: 'Força',
    rpm: 'RPM',
    status: {
      inDowntime: 'Em parada',
      inAlert: 'Em alerta',
      inOperation: 'Em operação',
    },
  },
};
