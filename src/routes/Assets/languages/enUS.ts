import ShowAssetTranslations from '../features/ShowAsset/languages/enUS';

export default {
  showAsset: ShowAssetTranslations,
  title: 'Assets',
};
