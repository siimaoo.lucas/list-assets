import ShowAssetTranslations from '../features/ShowAsset/languages/ptBR';

export default {
  showAsset: ShowAssetTranslations,
  title: 'Ativos',
};
