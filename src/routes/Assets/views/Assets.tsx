import React from 'react';
import { ListItem } from 'common/features/ListItem/containers';
import { useNavigate } from 'react-router-dom';
import { useTitle } from 'common/hooks';
import { useTranslation } from 'react-i18next';
import { ListAssetsCard } from '../features/ShowAsset/components';

function Assets() {
  const navigate = useNavigate();
  const { t } = useTranslation('assets');
  useTitle(t('title'));

  return (
    <ListItem
      requestURL="/assets"
      onClick={(record: { id: string | number }) => {
        navigate(`${record.id}`);
      }}
      Component={ListAssetsCard}
    />
  );
}

export default Assets;
