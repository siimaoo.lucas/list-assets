import React from 'react';
import { ListItem } from 'common/features/ListItem/containers';
import { useTitle } from 'common/hooks';
import { useTranslation } from 'react-i18next';
import { CardItem } from 'common/components';

const Card = ({
  type,
  data,
}: {
  type: 'card' | 'list';
  data: { name: string };
}) => (
  <CardItem
    type={type}
    title={data.name}
  />
);

function Companies() {
  const { t } = useTranslation('companies');
  useTitle(t('title'));
  return (
    <ListItem
      requestURL="/companies"
      Component={Card}
    />
  );
}

export default Companies;
