import React from 'react';
import { Card } from 'antd';

import { ROUTES_DATA } from 'common/components/Layout/Layout';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useTitle } from 'common/hooks';

function Home() {
  const { t: th } = useTranslation('home');
  const { t } = useTranslation('common');

  useTitle(th('title'));
  return (
    <section style={{ display: 'flex', gap: '1rem', flexDirection: 'column' }}>
      {Object.values(ROUTES_DATA).map(({ title, path, icon }) => (
        <Link
          key={title}
          to={path}
        >
          <Card style={{ borderRadius: '1rem' }}>
            <div
              style={{
                display: 'flex',
                gap: '1rem',
                alignItems: 'center',
                fontSize: '1.2rem',
              }}
            >
              {icon}
              {t(title)}
            </div>
          </Card>
        </Link>
      ))}
    </section>
  );
}

export default Home;
