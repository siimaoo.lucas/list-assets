import React from 'react';
import { ListItem } from 'common/features/ListItem/containers';
import { useTitle } from 'common/hooks';
import { useTranslation } from 'react-i18next';

function Units() {
  const { t } = useTranslation('units');
  useTitle(t('title'));
  return <ListItem requestURL="/units" />;
}

export default Units;
