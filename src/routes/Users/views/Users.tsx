import React from 'react';
import { ListItem } from 'common/features/ListItem/containers';
import { useTitle } from 'common/hooks';
import { useTranslation } from 'react-i18next';

function Users() {
  const { t } = useTranslation('users');
  useTitle(t('title'));
  return <ListItem requestURL="/users" />;
}

export default Users;
